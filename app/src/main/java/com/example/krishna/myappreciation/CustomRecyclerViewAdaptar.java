package com.example.krishna.myappreciation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomRecyclerViewAdaptar extends RecyclerView.Adapter<CustomRecyclerViewAdaptar.SimpleViewHolder> {

    ArrayList<ModelForPastEvents> messageDetails;
    ArrayList<Integer> colorsdata;
    Context context;

    public CustomRecyclerViewAdaptar(Context context, ArrayList<ModelForPastEvents> messageDetails, ArrayList<Integer> colorsdata) {
        this.messageDetails = messageDetails;
        this.colorsdata=colorsdata;
        this.context = context;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_desin_layout,parent,false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.message.setText(messageDetails.get(position).getMessage());
        holder.name.setText(messageDetails.get(position).getName());
        holder.date.setText(messageDetails.get(position).getDate());
        holder.time.setText(messageDetails.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return messageDetails.size();
    }

    public class SimpleViewHolder extends RecyclerView.ViewHolder {

        TextView message, name, date, time;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            message=(TextView)itemView.findViewById(R.id.pastevents_message);
            name=(TextView)itemView.findViewById(R.id.pastevents_nme);
            date=(TextView)itemView.findViewById(R.id.pastevents_date);
            time=(TextView)itemView.findViewById(R.id.pastevents_time);

        }
    }
}
