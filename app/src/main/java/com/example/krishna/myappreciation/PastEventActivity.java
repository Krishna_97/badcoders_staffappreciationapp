package com.example.krishna.myappreciation;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PastEventActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    ArrayList<Integer> colorsdata=new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Msg");

    ArrayList<ModelForPastEvents> messageDetails = new ArrayList<>();

    int [] colors={R.color.cardyellow,R.color.cardblue,R.color.cardgreen,R.color.cardorange,R.color.cardpurple,R.color.cardred,
            R.color.cardviolet,R.color.cardbrown,R.color.cardskyblue,R.color.cardpink};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_event);

        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        ModelForPastEvents obj=new ModelForPastEvents("krishna","message","12-05-2018","12.00 pm");
//
//        messageDetails.add(obj);
//        messageDetails.add(obj);
//        messageDetails.add(obj);
//        messageDetails.add(obj);




        adapter=new CustomRecyclerViewAdaptar(PastEventActivity.this, messageDetails,colorsdata);
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                messageDetails.clear();
                colorsdata.clear();
                for(DataSnapshot data:dataSnapshot.getChildren()){
                    messageDetails.add(data.getValue(ModelForPastEvents.class));
                }
                if(messageDetails.size()>10){
                    for(int i=0;i<messageDetails.size();i++) {
                            colorsdata.add(colors[i%10]);
                    }
                } else {
                    for(int i=0;i<messageDetails.size();i++) {
                        colorsdata.add(colors[i]);
                    }
                }
                Log.d("msg", "message count: "+messageDetails.size());
                adapter=new CustomRecyclerViewAdaptar(PastEventActivity.this, messageDetails,colorsdata);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
