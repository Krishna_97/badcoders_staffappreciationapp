package com.example.krishna.myappreciation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    View rootView;

//    ProgressBar prg;
//    RatingBar rate;
//    TextView prgtxt;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

//        prg= (ProgressBar)rootView.findViewById(R.id.prog);
//        rate=(RatingBar)rootView.findViewById(R.id.rating);
//        prgtxt=(TextView)rootView.findViewById(R.id.progtxt);
//        prgtxt.setText(""+prg.getProgress()+"%");

        return rootView;
    }

//    public void fun(View v){
//
//        if(v.getId()==R.id.plus){
//            int x = prg.getProgress();
//            x++;
//            prg.setProgress(x);
//            prgtxt.setText(""+prg.getProgress()+"%");
//            inc();
//        }
//        if(v.getId()==R.id.minus){
//            int x = prg.getProgress();
//            x--;
//            prg.setProgress(x);
//            prgtxt.setText(""+prg.getProgress()+"%");
//            dec();
//        }
//    }
//    public void inc(){
//        rate.setRating(2);
//    }
//    public void dec(){
//        rate.setRating(1);
//    }

}
