package com.example.krishna.myappreciation;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class EventsAdapter extends BaseExpandableListAdapter {
    List<String> parent_headings;
    HashMap<String,List<ModelForEvents>> child_titles;
    Context context;

    public EventsAdapter(List<String> parent_headings, HashMap<String, List<ModelForEvents>> child_titles, Context context) {
        this.parent_headings = parent_headings;
        this.child_titles = child_titles;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return parent_headings.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return child_titles.get(parent_headings.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_headings.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        String[] child_details={child_titles.get(parent_headings.get(i)).get(i1).getTitle(),
                child_titles.get(parent_headings.get(i)).get(i1).getDate(),
                child_titles.get(parent_headings.get(i)).get(i1).getTime()};

        return child_details;
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String title=(String)this.getGroup(i);
        if(view==null){
            LayoutInflater layoutInflater=(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.parent_layout,null);
        }
        TextView heading=(TextView)view.findViewById(R.id.parent_heading);
        heading.setText(title);

        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        String[] details=(String[])this.getChild(i,i1);
        Log.d("msg","title:"+details[0]+"date:"+details[1]+"time:"+details[2]);
        if(view==null){
            LayoutInflater layoutInflater=(LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=layoutInflater.inflate(R.layout.child_layout,null);
        }
        TextView heading=view.findViewById(R.id.child_heading);
        heading.setText(details[0]);
        TextView date=view.findViewById(R.id.child_date);
        date.setText(details[1]);
        TextView time=view.findViewById(R.id.child_time);
        time.setText(details[2]);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
