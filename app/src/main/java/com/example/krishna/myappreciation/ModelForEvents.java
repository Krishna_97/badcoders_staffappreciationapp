package com.example.krishna.myappreciation;

public class ModelForEvents {
    String title;
    String date;
    String time;

    public ModelForEvents(String title, String date, String time) {
        this.title = title;
        this.date = date;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }


}
