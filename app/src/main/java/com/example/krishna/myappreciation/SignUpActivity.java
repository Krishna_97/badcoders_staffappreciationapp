package com.example.krishna.myappreciation;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;

public class SignUpActivity extends AppCompatActivity {

    TextInputLayout uid;
    TextInputLayout pwd;
    TextInputLayout nm;
    TextInputLayout em;
    TextInputLayout cn;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        uid = findViewById(R.id.signup_user_id);
        pwd= findViewById(R.id.passwrd);
        nm = findViewById(R.id.name);
        em = findViewById(R.id.email);
        cn = findViewById(R.id.c_no);
        btn =(Button)findViewById(R.id.signup);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate_Signup()){
                    Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

    }


    public boolean validate_Signup()
    {
        String inputUID = uid.getEditText().getText().toString();
        String inputPassword = pwd.getEditText().getText().toString();
        String inputName = nm.getEditText().getText().toString();
        String inputEmail = em.getEditText().getText().toString();
        String inputNumber = cn.getEditText().getText().toString();

        if(inputUID.isEmpty())
        {
            uid.setError("UserId field cannot be empty");
            return false;
        }
        else if (inputPassword.isEmpty())
        {
            pwd.setError("Password field cannot be empty");
            return  false;
        }
        else if (inputName.isEmpty())
        {
            nm.setError("Mother Name feild cannot be empty");
            return  false;
        }
        else if (inputEmail.isEmpty())
        {
            em.setError("Email field cannot be empty");
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(inputEmail).matches())
        {
            em.setError("Enter valid email address");
            return  false;
        }
        else if (inputNumber.isEmpty())
        {
            nm.setError("Contact No. field cannot be empty");
            return false;
        }
        else if (inputNumber.length()<10)
        {
            nm.setError("Enter valid Contact No.");
            return false;

        }
        else
            return true;
    }


}
