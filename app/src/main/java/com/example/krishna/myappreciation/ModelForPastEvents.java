package com.example.krishna.myappreciation;

public class ModelForPastEvents {
    String name1, message, date, time;

    public ModelForPastEvents(String name, String message, String date, String time) {
        this.name1 = name;
        this.message = message;
        this.date = date;
        this.time = time;
    }

    public String getName() {
        return name1;
    }

    public String getMessage() {
        return message;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}
