package com.example.krishna.myappreciation;

import android.os.Binder;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class MainActivity extends AppCompatActivity {

    FrameLayout fragmentContainer;
    FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView =findViewById(R.id.bottom_navigation_bar);
        fragmentManager = getSupportFragmentManager();

        fragmentContainer = findViewById(R.id.fragment_container);

        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container,new ProfileFragment(),null);

        fragmentTransaction.commit();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( MainActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                switch (item.getItemId()){
                    case R.id.profile:
                        fragmentTransaction.replace(R.id.fragment_container,new ProfileFragment(),null).commit();
                        return true;
                    case R.id.events:
                        fragmentTransaction.replace(R.id.fragment_container,new EventsFragment(),null).commit();
                        return true;
                    case R.id.box:
                        fragmentTransaction.replace(R.id.fragment_container,new BoxFragment(),null).commit();
                        return true;
                }
                return false;
            }
        });
    }
}
