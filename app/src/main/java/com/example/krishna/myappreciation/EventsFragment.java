package com.example.krishna.myappreciation;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {

    View rootView;
    ExpandableListView expandableListView;
    HashMap<String,List<ModelForEvents>> child_items=new HashMap<>();
    List<String> headings=new ArrayList<>();
    List<ModelForEvents> upcomingItems=new ArrayList<>();
    List<ModelForEvents> pastItems=new ArrayList<>();
    EventsAdapter eventsAdapter;
    private  TextView[] mdots;
    EventsSliderAdapter eventsSliderAdapter;
    ArrayList<String> imageUrls = new ArrayList<>();
    ViewPager eventsViewPager;
    static boolean bool=false;

    static Timer timer;
    static EventsSliderTimer thread;

    LinearLayout eventsSliderDotPanel;


    public EventsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        headings.clear();
        headings.add("Upcoming Events");
        headings.add("Past Events");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_events, container, false);

        eventsSliderDotPanel = rootView.findViewById(R.id.events_slider_dots);

        expandableListView=rootView.findViewById(R.id.exp_lv);

        child_items.put(headings.get(0),upcomingItems);
        child_items.put(headings.get(1),pastItems);

        ModelForEvents pastEvent1 = new ModelForEvents("Past1","12/132/78","12:00");
        ModelForEvents pastEvent2 = new ModelForEvents("past2","12/132/78","12:00");
        ModelForEvents upEvent1 = new ModelForEvents("upcoming1","12/132/78","12:00");
        ModelForEvents upEvent2 = new ModelForEvents("upcoming2","12/132/78","12:00");


        upcomingItems.add(upEvent1);
        upcomingItems.add(upEvent2);
        pastItems.add(pastEvent1);
        pastItems.add(pastEvent2);

        eventsAdapter=new EventsAdapter(headings,child_items,getContext());
        expandableListView.setAdapter(eventsAdapter);



        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

                Intent intent;
                if (headings.get(i).equals("Upcoming Events")) {

                    intent = new Intent(getContext(), UpComingEventActivity.class);
                } else {
                    intent = new Intent(getContext(), PastEventActivity.class);
                }
                startActivity(intent);

//                Toast.makeText(getContext(),child_items.get(headings.get(i)).get(i1)+" clicked",Toast.LENGTH_SHORT).show();
//                Intent intent=new Intent(getContext(),EventsActivity.class);
//                intent.putExtra("Event Name",child_items.get(headings.get(i)).get(i1));
//                for(int j=0;j<EventDetailsList.size();j++){
//                    if(EventDetailsList.get(j).getEventName().equals(child_items.get(headings.get(i)).get(i1))){
//                        String eventDesc=EventDetailsList.get(j).getEventDesc();
//                        String eventImage=EventDetailsList.get(j).getEventImage();
//                        intent.putExtra("Event Desc",eventDesc);
//                        intent.putExtra("Event Image",eventImage);
//                        break;
//                    }
//                }
//                startActivity(intent);
//                return true;
                return true;
            }
        });

        imageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0AUIsr_xwhC00R0P9Lyy5COhN_ZRBzHxLZJSJbjyCqMuCb2L53g");
        imageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRN2cMT8pIV1HIG-M1HhEdp3cU36uD3AYylwYN1SS9kT2z5_FN7");
        imageUrls.add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPqzA1_lhRxrgpi0Aqx3eiFTDBSFd8cAc3OiTLboF2zNnc2I_14A");
        eventsViewPager = rootView.findViewById(R.id.events_view_pager);

        eventsSliderAdapter = new EventsSliderAdapter(getContext(), imageUrls);
        eventsViewPager.setAdapter(eventsSliderAdapter);

        eventsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                addDotsIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bool=true;
        if (bool){
            timer = new Timer();
            thread =new EventsSliderTimer();
            timer.scheduleAtFixedRate(thread, 3000,5000);
        }

        return rootView;
    }

    public void addDotsIndicator(int position) {
        mdots = new TextView[imageUrls.size()];
        eventsSliderDotPanel.removeAllViews();
        for (int i = 0; i < imageUrls.size(); i++) {
            mdots[i] = new TextView(getContext());
            mdots[i].setText(Html.fromHtml("&#8226;"));
            mdots[i].setTextSize(35);
            mdots[i].setTextColor(getResources().getColor(R.color.colorPrimary));
            eventsSliderDotPanel.addView(mdots[i]);
        }
        if (mdots.length > 0) {
            mdots[position].setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public class EventsSliderTimer extends TimerTask {

        @Override
        public void run() {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(eventsViewPager.getCurrentItem() == imageUrls.size()-1){
                        eventsViewPager.setCurrentItem(0);
                    }else{
                        eventsSliderAdapter.notifyDataSetChanged();
                        eventsViewPager.setCurrentItem(eventsViewPager.getCurrentItem()+1);
                    }
                }
            });
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bool=false;
        timer.cancel();
        thread.cancel();
    }

    @Override
    public void onStop() {
        super.onStop();
        bool=false;
        timer.cancel();
        thread.cancel();
    }

}
