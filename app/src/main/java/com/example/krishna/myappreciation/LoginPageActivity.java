package com.example.krishna.myappreciation;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginPageActivity extends AppCompatActivity {

    Button btn;
    private TextInputLayout textInputUsername;
    private TextInputLayout textInputPassword;
    TextView newUserTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        textInputUsername = findViewById(R.id.text_input_username);

        textInputPassword = findViewById(R.id.text_input_password);

        newUserTextView = findViewById(R.id.new_user_tv);

        btn= findViewById(R.id.login_btn);


        textInputUsername.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textInputUsername.setError(null);
            }
        });

        textInputPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                textInputPassword.setError(null);
            }
        });




        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()){
                    Intent intent = new Intent(LoginPageActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        newUserTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginPageActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
    }


    public boolean validate()
    {
        String passwordInput = textInputPassword.getEditText().getText().toString().trim();
        String usernameInput = textInputUsername.getEditText().getText().toString().trim();

        if(passwordInput.isEmpty() && usernameInput.isEmpty())
        {
            textInputPassword.setError("Password Can't be Empty");
            textInputUsername.setError("Username Can't Be empty");
            return false;
        }

        else if (usernameInput.length()>15)
        {
            textInputUsername.setError(null);
            textInputPassword.setError(null);
            textInputUsername.setError("Username is too long");
            return false;
        }

        else if(usernameInput.isEmpty())
        {
            textInputUsername.setError(null);
            textInputPassword.setError(null);
            textInputUsername.setError("Username Can't be Empty");
            return false;
        }

        else if(passwordInput.isEmpty())
        {
            textInputUsername.setError(null);
            textInputPassword.setError(null);
            textInputPassword.setError("Password Can't be Empty");
            return false;
        }

        return true;
    }
}
